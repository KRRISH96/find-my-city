import React, { Component } from 'react';
import CityList from './components/CityList'
import './App.css';

class App extends Component {
  state={
    zipCode: '',
    currentList: [],
  }

  handleZipCodeChange = (e) => {
    this.setState({
      zipCode: e.target.value,
    })
  }

  getCities = (zipCode) => {
    fetch(`https://api.zippopotam.us/us/${zipCode}`)
      .then(res => res.json())
      .then(res => {
        const place = res.places[0];
        this.setState(prevState => ({
          currentList: [...prevState.currentList, {place, zipCode}]
        }))
      });
  }

  findCities = (e) => {
    e.preventDefault();
    const { zipCode } = this.state;
    const uniqueCodes = this.state.currentList.filter(place => place.zipCode === zipCode);
    if(uniqueCodes.length<1) {
      this.getCities(zipCode);
    }
    this.setState({
      zipCode: '',
    })
  }
    
    
    render() {
    const { currentList } = this.state;
    return (
      <div className="App">
        <p>Enter the zipcode</p>
        <form onSubmit={this.findCities}>
          <label htmlFor="city-finder-input">
            <input
              type='number'
              id="city-finder-input"
              placeholder="Ex: 123456"
              onChange={this.handleZipCodeChange}
              value={this.state.zipCode}
            />
            <button>Go</button>
          </label>
        </form>
        <CityList places={currentList} />
      </div>
    );
  }
}

export default App;
