import React, { Component } from 'react';

class CityList extends Component {
  render() {
    const { places } = this.props;
    return (
      <div>
        { places && places.map(({place, zipCode}) => (
            <div  key={zipCode}>
              <p>{place['place name']}&#44; {`${place['state abbreviation']}(${zipCode})`}</p>
            </div>
          ))
        }
      </div>
    );
  }
}

export default CityList;